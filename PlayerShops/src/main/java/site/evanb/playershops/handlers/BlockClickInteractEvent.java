package site.evanb.playershops.handlers;

import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.text.Text;

import site.evanb.playershops.events.ShopTransactionEvent;

public class BlockClickInteractEvent {

	@Listener
	public void onSignInteractEvent(InteractBlockEvent.Secondary event, @First Player p) {
		BlockSnapshot bs = event.getTargetBlock();
		
		if(bs.getExtendedState().getType().equals(BlockTypes.WALL_SIGN)) {
			event.setCancelled(true);
			Sign sign = (Sign) bs.getLocation().get().getTileEntity().get();
			
			if(!sign.get(SignData.class).get().get(0).isPresent()) {
				return;
			}
			for(Text text : sign.get(SignData.class).get().asList()) {
				p.sendMessage(text);
			}
			
			
			
			if(sign.getSignData().get(0).toString() == "[BUY]") {
				ShopTransactionEvent shopEvent = new ShopTransactionEvent(p, null);
			}
			
			if(sign.getSignData().get(0).toString() == "[SELL]") {
				ShopTransactionEvent shopEvent = new ShopTransactionEvent(p, null);				
			}
			
			//if(sign.get(SignData.class).get().get(0).get().toPlain().toUpperCase().equals("[BUY]")) {
			
			
		} else {
			return;
		}
		
	}
}
