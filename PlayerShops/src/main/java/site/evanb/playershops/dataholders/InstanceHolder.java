package site.evanb.playershops.dataholders;

import site.evanb.playershops.PlayerShops;

public class InstanceHolder {

	private PlayerShops instance;
	
	private static Shops shops;
	
	
	public InstanceHolder(PlayerShops instance) {
		this.instance = instance;
		InstanceHolder.shops = new Shops(this.instance);
	}
	
	public PlayerShops getInstance() {
		return instance;
	}
	
	public Shops getShopsClass() {
		return shops;
	}
}
