package site.evanb.playershops.dataholders;

import java.util.UUID;

import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import site.evanb.playershops.PlayerShops;

public class Shop {
	
	public enum ShopType{
		ADMIN_SHOP,
		PLAYER_SHOP;
	};

	public enum ShopAction{
		BUY,
		SELL;
	};
	
	private Location<World> loc;
	private User owner;
	private ShopAction action;
	private ShopType type;
	private ItemStack item;
	private double price;
	
	public Shop(Location<World> loc, User owner, ShopAction action, ShopType type, ItemStack item, double price) {
		this.loc = loc;
		this.owner = owner;
		this.action = action;
		this.type = type;
		this.item = item;
		this.price = price;
	} 
	
	public Shop(PlayerShops plugin, String worldName, int x, int y, int z) {
		plugin.getInstance().getInstances().getShopsClass().getFromLocation(worldName, x, y, z);
	}
	
	
	
	public Location<World> getLocation(){
		return this.loc;
	}
	
	public String getOwnerName() {
		return this.owner.getName();
	}
	
	public UUID getOwnerUUID() {
		return this.owner.getUniqueId();
	}
	
	public User getOwner() {
		return this.owner;
	}
	
	public ShopType getType() {
		return this.type;
	}
	
	public ShopAction getAction() {
		return this.action;
	}
	
	public ItemStack getItemStack() {
		return this.item;
	}
	
	public double getPrice() {
		return this.price;
	}
	
	public int getQuantity() {
		return item.getQuantity();
	}

}
