package site.evanb.playershops.events;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Cancellable;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.impl.AbstractEvent;

import site.evanb.playershops.dataholders.Shop;

public class ShopTransactionEvent extends AbstractEvent implements Cancellable {	
	
	private boolean active = true;
	private Player cause;
	private Shop shop;
	
	public ShopTransactionEvent(Player cause, Shop shop) {
		this.cause = cause;
		this.shop = shop;
		
	}

	@Override
	public Cause getCause() {
		return null;
		/*try (CauseStackManager.StackFrame frame = Sponge.getCauseStackManager().pushCauseFrame()){
			frame.pushCause(cause);
			return frame.getCurrentCause();
		}*/
	}

	@Override
	public boolean isCancelled() {
		return (!active);
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.setCancelled((!cancel));
	}

}
