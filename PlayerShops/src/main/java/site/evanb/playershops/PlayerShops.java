package site.evanb.playershops;

import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameStartingServerEvent;
import org.spongepowered.api.plugin.Plugin;

import com.google.inject.Inject;

import site.evanb.playershops.dataholders.InstanceHolder;
import site.evanb.playershops.handlers.BlockClickInteractEvent;

@Plugin(name="Player Shops", version="0.1-Beta", description="Player Shops The Way They Should Be Done", id = "playershops")
public class PlayerShops {
	
	@Inject
	private static Logger logger;
	
	private InstanceHolder instanceHolder = new InstanceHolder(this);
	
	public static Logger getLogger() {
		return logger;
	}
	
	@Listener
	public void onServerStart(GameStartingServerEvent e) {
		Sponge.getEventManager().registerListeners(this, new BlockClickInteractEvent());
		logger.info("Player Shops Loaded.");	
	}

	public PlayerShops getInstance() {
		return this;
	}
	
	public InstanceHolder getInstances() {
		return instanceHolder;
	}
}
